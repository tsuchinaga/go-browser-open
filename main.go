package main

import (
	"fmt"
	"log"
	"net"
	"net/http"
	"os/exec"
)

func main() {
	log.SetFlags(log.LstdFlags | log.Llongfile)

	// 空いてるポートを取る
	ln, err := net.Listen("tcp", "127.0.0.1:0")
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Println(fmt.Sprintf(`"http://%s"`, ln.Addr()))

	// 空いてるポートでWebサーバの待ち受けを開始する
	mux := http.NewServeMux()
	mux.HandleFunc("/", func(writer http.ResponseWriter, _ *http.Request) {
		_, _ = writer.Write([]byte("Hello, World"))
	})
	srv := http.Server{Handler: mux}
	go srv.Serve(ln)

	// 建てたWebサーバをブラウザで開く
	cmd := exec.Command("rundll32.exe", "url.dll,FileProtocolHandler", fmt.Sprintf(`http://%s`, ln.Addr()))
	if err := cmd.Run(); err != nil {
		log.Fatalln(err)
	} else {
		fmt.Println("run executed")
	}

	// ブロッキング
	select {}
}
